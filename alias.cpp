#include "alias.h"

Alias::Alias() {

}

Alias::~Alias() {

}

QTextStream &operator <<(QTextStream &out, const Alias &alias) {
    out << alias.m_dateTime
        << alias.m_text;
    return  out;
}

QTextStream &operator >>(QTextStream &in, Alias &alias) {
    alias.m_dateTime = in.readLine();
    do {
        alias.m_text = in.readLine();
    }
    while(alias.m_text.isEmpty());

    return in;
}
