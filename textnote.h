#ifndef TEXTNOTE_H_
#define TEXTNOTE_H_


#include <QDateTime>
#include <QStringList>
#include <QTextStream>


class TextNote {

public:
    TextNote();//set time, set text = ""
    TextNote(const QString &noteText);//set time and text = noteText
   ~TextNote();

    QString & getTextNoteText(); //return text of note
    QString & getTextNoteDateTime(); // return date and time for current textnote

    friend QTextStream &operator <<(QTextStream &out, const TextNote &note);
    friend QTextStream &operator >>(QTextStream &in, TextNote &note);

private:
    inline QString currentDateTime() const; //return current date and time

private:
    QString m_textNoteText;
    QString m_textNoteDateTime;

};

QTextStream &operator <<(QTextStream &out, const TextNote &note);
QTextStream &operator >>(QTextStream &in, TextNote &note);

#endif  //TEXTNOTE_H_
