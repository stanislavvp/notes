#include <book.h>
#include <QDebug>

Book::Book() {
    createDir();
    m_selectedNote = m_selectedNote.create();
}

Book::~Book() {

}

void Book::createDir() {
    if(!QDir().exists(m_dirName)) {
        QDir().mkdir(m_dirName);
    }
    fillNotesList();
}

QStringList Book::getFilesList() {
    if(QDir().exists(m_dirName)) {
        QDir dir(m_noteFilePath);
        QStringList noteFiles = dir.entryList(QDir::Files, QDir::Time | QDir::Reversed);
        for(auto &filename : noteFiles) {
            filename = m_noteFilePath + filename;                             //add path to name because files are not in deafault project folder
        }
        return noteFiles;
    }
    else return QStringList();
}



void Book::fillNotesList() {
    if(!m_notesList.isEmpty()) {                                               //clear because this method will used to update notes list
        m_notesList.clear();
    }
    QStringList noteFiles = getFilesList();
        for(const auto &file : noteFiles) {
            QFile currentFile(file);
            currentFile.open(QIODevice::ReadOnly);
            if(currentFile.isOpen()) {
                QTextStream in(&currentFile);
                auto noteAlias = QSharedPointer<Alias>::create();
                in >> *noteAlias;
                currentFile.close();
                m_notesList.push_back(noteAlias);
        }
    }
}

QList<QSharedPointer<Alias>> Book::getNotesList() const {
    return m_notesList;
}

TextNote & Book::getSelectedNote() const {
    return *m_selectedNote.get();
}
                                                                              //create and init from file
bool Book::setSelectedNote(const int &row) {
    QStringList filesList = getFilesList();
    QFile selectedFile(filesList.at(row));
    selectedFile.open(QIODevice::ReadOnly);
    if(selectedFile.isOpen()) {
        QTextStream in(&selectedFile);
        in >> *m_selectedNote;
        selectedFile.close();
        return true;
    }
    return false;
}

void Book::resetSelectedNote() {
    m_selectedNote.reset();
}

bool Book::createNote(const QString &text) {
    m_selectedNote = m_selectedNote.create(text);
    QFile newNoteFile(m_noteFilePath + m_selectedNote->getTextNoteDateTime());
    newNoteFile.open(QIODevice::NewOnly);
    if(newNoteFile.isOpen()) {
        QTextStream out(&newNoteFile);
        out << *m_selectedNote;
        newNoteFile.close();
        fillNotesList();
        return true;
    }
    return false;
}


                                                                              //delete file from folder and rewrite notes list
bool Book::removeNote(const int &row) {
    QStringList noteFiles = getFilesList();
    if(QDir().exists(noteFiles.at(row))) {
        QDir().remove(noteFiles.at(row));
        fillNotesList();
        return true;
    }
    return false;
}

bool Book::changeNote() {
    if(!m_selectedNote){
        return false;
    }
    QFile selectedNoteFile(m_noteFilePath + m_selectedNote->getTextNoteDateTime());
    selectedNoteFile.open(QIODevice::WriteOnly);
    if(selectedNoteFile.isOpen()){
        QTextStream out(&selectedNoteFile);
        out << *m_selectedNote;
        selectedNoteFile.close();
        fillNotesList();
        return true;
    }
    return false;
}




