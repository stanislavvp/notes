import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Dialogs 1.3


Dialog {
    id:dialog
     width: mainWindow.width    //coment when deploy on android
     height: mainWindow.height   //coment when deploy on android

    contentItem: Rectangle {
        id:dialogContent
        width: mainWindow.width
        height: mainWindow.height
        anchors.centerIn: parent

        Flickable {
               id: flickable
               flickableDirection: Flickable.VerticalFlick
               height: parent.height - saveButton.height
               width: parent.width
               anchors.bottom: saveButton.top
               TextArea.flickable: TextArea {
                   id: textArea
                   textFormat: Qt.RichText
                   font.pixelSize: mainWindow.height / 25
                   wrapMode: TextArea.Wrap
                   focus: true
                   selectByMouse: true
                   persistentSelection: true
                   leftPadding: 6
                   rightPadding: 6
                   topPadding: 0
                   bottomPadding: 0
                   background: Rectangle {
                       width: parent.width
                       height: parent.height
                       anchors.centerIn: parent
                       color: "white"
                   }
               }
        }

        Button {
            id:saveButton
            anchors {bottom: parent.bottom}
            height: parent.height / 13
            width: parent.width
             Material.background: Material.color(Material.Green, Material.Shade400)

            Text {
                text: "SAVE NOTE"
                font.pixelSize: parent.height / 2
                font.bold: true
                color: "white"
                anchors.centerIn: parent
            }

            onClicked: editWindow.close()
        }
    }

}
