import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Controls.Styles 1.4
import NotesApplicationModel 1.0

Button {
    visible: false
    anchors {bottom: parent.bottom}
    height: parent.height / 13
    width: parent.width
    Material.background: Material.color(Material.Red, Material.Shade400)

    Text {
        text: "DELETE"
        font.pixelSize: parent.height / 2
        font.bold: true
        color: "white"
        anchors.centerIn: parent
    }
}
