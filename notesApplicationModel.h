#ifndef NOTESAPPLICATIONMODEL_H_
#define NOTESAPPLICATIONMODEL_H_

#include <QAbstractListModel>
#include "book.h"

class NotesApplicationModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum Roles {
        noteTextRole = Qt::UserRole + 1,
        noteDateTimeRole,
        noteIsSelectedRole
    };

    NotesApplicationModel(QObject *parent = nullptr);
   ~NotesApplicationModel() override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;                               //returns count of rows in given parent
    QHash<int, QByteArray> roleNames() const override;                                                    //returns role names for model(sets own role names)
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;                   //returns the data stored under given role, for item by model index
    bool setData(const QModelIndex &index, const QVariant &value, const int role = Qt::EditRole) override;//sets role for item at index - to value
    Qt::ItemFlags flags(const QModelIndex &index) const override;                                         //returns flags for given index

    Q_INVOKABLE const QString & read(const int row);                                                      //read text data of selected note from file
    Q_INVOKABLE void add(const QVariant &item);                                                           //add new note
    Q_INVOKABLE void remove(const int row);                                                               //remove selected notes


private:
    QSharedPointer<Book> m_book;

};

#endif //NOTESAPPLICATIONMODEL_H_
