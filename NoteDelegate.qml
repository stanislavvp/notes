import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import NotesApplicationModel 1.0

Rectangle {
    id: noteItem
    width: view.width
    height: view.height / 7
    color: Material.color(Material.Lime, Material.Shade100)

    property string noteName: model.noteName
    property string noteDateTime: model.noteDateTime
    property bool noteIsSelected: model.noteIsSelected


    Text {
        id: delegateName
        font.pixelSize: parent.height / 4
        width: parent.width * 0.8
        clip: true
        text: noteName
        anchors {
            left: parent.left
            leftMargin: parent.width / 20
            bottom: parent.bottom
            bottomMargin: parent.height / 2
        }

    }

    Text {
        id: delegateDataTime
        font.pixelSize: parent.height / 5
        text: noteDateTime
        anchors {
            left: parent.left
            leftMargin: parent.width / 20
            bottom: parent.bottom
            bottomMargin: parent.height / 8
        }
    }

    MouseArea {
        id: itemMouseArea
        anchors.fill: parent
        onPressAndHold: deleteButton.visible = true
        onClicked: function() {
            if(deleteButton.visible === false) {
                editWindow.open()
            }
            else {
                deleteButton.visible = false
            }
        }
    }

    RoundButton {
        id: deleteButton
        visible: false
        radius: parent.height / 2
        anchors.right: parent.right
        anchors.rightMargin: parent.width / 30
        anchors.verticalCenter: parent.verticalCenter
        Material.background: Material.color(Material.Red, Material.Shade400)
        Image {
            source: "resources/trashCan.png"
            anchors.centerIn: parent
            width: parent.width / 1.5
            height: parent.height / 1.5
        }
        onClicked: function(){
            dataModel.remove(model.index)
        }

    }

}
