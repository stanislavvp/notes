import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3


Button {
    anchors {bottom: parent.bottom}
    height: parent.height / 13
    width: parent.width
    Material.background: Material.color(Material.Green, Material.Shade400)

    Text {
        text: "ADD NOTE"
        font.pixelSize: parent.height / 2
        font.bold: true
        color: "white"
        anchors.centerIn: parent
    }
}
