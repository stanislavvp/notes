#ifndef ALIAS_H_
#define ALIAS_H_

#include <QString>
#include <QTextStream>

class Alias {
public:
    Alias();
    ~Alias();

    friend QTextStream &operator <<(QTextStream &out, const Alias &alias);
    friend QTextStream &operator >>(QTextStream &out, Alias &alias);

public:
    QString  m_text;
    QString  m_dateTime;
    bool m_isSelected = false;
};


QTextStream &operator <<(QTextStream &out, const Alias &alias);
QTextStream &operator >>(QTextStream &in, Alias &alias);


#endif //Alias_H_
