import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import NotesApplicationModel 1.0


ApplicationWindow {
    id: mainWindow
    width: 480
    height: 640
    Material.accent: Material.Green
    visible: true

    NoteEditWindow {
        id:editWindow
    }

    NotesApplicationModel{
        id:dataModel
    }

    AddButton{
        id:addButton
      onClicked: dataModel.add("some text")
    }


    ListView{
        id:view
        property bool delSelect: false
        clip: true
        spacing: 5
        anchors.top: parent.top
        width: parent.width
        height: parent.height - addButton.height
        model: dataModel
        delegate: NoteDelegate{

        }

    }

}


