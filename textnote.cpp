#include "textnote.h"

TextNote::TextNote() {

    m_textNoteDateTime = "";

    m_textNoteText = "";
}

TextNote::TextNote(const QString &noteText) {

    m_textNoteDateTime = currentDateTime() + "\n\n";

    m_textNoteText = noteText;
}

TextNote::~TextNote() {

}


QString TextNote::currentDateTime() const {

    return QDateTime::currentDateTime().toString("dd.MM.yyyy-hh:mm:ss");
}


/*const*/ QString & TextNote::getTextNoteText() {

    return m_textNoteText;
}

/*const*/ QString & TextNote::getTextNoteDateTime() {

    return m_textNoteDateTime;
}


QTextStream &operator <<(QTextStream &out, const TextNote &note) {
    out << note.m_textNoteDateTime;
    out << note.m_textNoteText;
    return out;
}

QTextStream &operator >>(QTextStream &in, TextNote &note) {
    note.m_textNoteDateTime = in.readLine();
    note.m_textNoteText = in.readAll();
    return in;
}






