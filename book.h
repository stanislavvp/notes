#ifndef BOOK_H_
#define BOOK_H_


#include <QDir>
#include <QFile>
#include <QAbstractListModel>
#include <QSharedPointer>
#include "textnote.h"
#include "alias.h"

class Book  {

public:
    Book();  // if cant find directory , create directory for notes||fill m_textnotes
   ~Book();
    bool createNote(const QString& text);//creates note,push it to list, save it to  file in dir
    bool removeNote(const int &row);//remove file from dir, remove alias from list;
    bool setSelectedNote(const int &row);//set m_selectedNote from file. identifier = row in model
    void resetSelectedNote(); //sets m_selectedNote empty Note
    bool changeNote(); //save changes;
    QList<QSharedPointer<Alias>> getNotesList() const;
    TextNote & getSelectedNote() const;//return pointer to seleted note

private:
    void createDir(); //creates directory for notes mkdir()
    QStringList getFilesList();//return list of files in dir
    void fillNotesList();  //fill m_NoteList from files in dir


private:
    QList<QSharedPointer<Alias>> m_notesList;
    QSharedPointer<TextNote> m_selectedNote;
    const QString m_noteFilePath = "MyNotes/";
    const QString m_dirName = "MyNotes";
};


#endif  //BOOK_H_
