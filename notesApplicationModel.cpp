#include "notesApplicationModel.h"
#include "QVariant"

NotesApplicationModel::NotesApplicationModel(QObject *parent) : QAbstractListModel(parent) {
    m_book = m_book.create();
}

NotesApplicationModel::~NotesApplicationModel() {

}

int NotesApplicationModel::rowCount(const QModelIndex &parent) const {
    if(parent.isValid()){
        return 0;
    }
    return m_book->getNotesList().size();
}

QVariant NotesApplicationModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid()) {
        return QVariant();
    }
    switch (role) {
    case noteTextRole:
        return m_book->getNotesList().at(index.row())->m_text;
    case noteDateTimeRole:
        return m_book->getNotesList().at(index.row())->m_dateTime;
    case noteIsSelectedRole:
        return m_book->getNotesList().at(index.row())->m_isSelected;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> NotesApplicationModel::roleNames() const {
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[noteTextRole] = "noteName";
    roles[noteDateTimeRole] = "noteDateTime";
    roles[noteIsSelectedRole] = "noteIsSelected";
    return roles;
}

bool NotesApplicationModel::setData(const QModelIndex &index, const QVariant &value, const int role) {
    if(!index.isValid() && !roleNames().contains(role)) {
        return false;
    }

    switch (role) {
    case noteTextRole : {
        m_book->setSelectedNote(index.row());   //create TextNote with data from file by index(files stored by time of creation)
        m_book->getSelectedNote().getTextNoteText() = value.toString();
        m_book->changeNote();
        m_book->resetSelectedNote();
        break;
    }
    case noteIsSelectedRole: {
        m_book->getNotesList().at(index.row())->m_isSelected = value.toBool();
        break;
    }
    default:
        return false;
    }
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
}

Qt::ItemFlags NotesApplicationModel::flags(const QModelIndex &index) const {
    if(!index.isValid()) {
        return Qt::NoItemFlags;
    }
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}

void NotesApplicationModel::remove(const int row) {
    if(index(row).isValid()){
    beginRemoveRows(QModelIndex(), row , row);
    m_book->removeNote(row);
    endRemoveRows();
    }
}

void NotesApplicationModel::add(const QVariant &item) {
    beginInsertRows(QModelIndex(), m_book->getNotesList().size(), m_book->getNotesList().size());
    m_book->createNote(item.toString());
    endInsertRows();
}

const QString & NotesApplicationModel::read(const int row) {
    m_book->setSelectedNote(row);
    return m_book->getSelectedNote().getTextNoteText();
}

